package com.citi.training.customers.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class CustomerTests {

	private int testId = 53;
    private String testName = "Michelle";
    private String testAddress = "Ahoghill";
    
    @Test
    public void test_Customer_constructor() {
        Customer testCustomer = new Customer(testId, testName, testAddress);;

        assertEquals(testId, testCustomer.getId());
        assertEquals(testName, testCustomer.getName());
        assertEquals(testAddress, testCustomer.getAddress());
    }

    @Test
    public void test_Customer_toString() {
        String testString = new Customer(testId, testName, testAddress).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testName));
        assertTrue(testString.contains(testAddress));
    }
}
