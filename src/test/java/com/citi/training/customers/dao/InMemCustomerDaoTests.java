package com.citi.training.customers.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.customers.exceptions.CustomerNotFoundException;
import com.citi.training.customers.model.Customer;

public class InMemCustomerDaoTests {

    private static Logger LOG = LoggerFactory.getLogger(InMemCustomerDaoTests.class);

    private int testId = 879;
    private String testName = "Tim Berg";
    private String testAddress = "109 frag lane";

    @Test
    public void test_saveAndGetCustomer() {
        Customer testCustomer = new Customer(testId, testName, testAddress);
        InMemCustomerDao testRepo = new InMemCustomerDao();

        testRepo.saveCustomer(testCustomer);
 
        assertTrue(testRepo.getCustomer(testId).equals(testCustomer));

    }

    @Test
    public void test_saveAndGetAllCustomers() {
        Customer[] testCustomerArray = new Customer[100];
        InMemCustomerDao testRepo = new InMemCustomerDao();

        for(int i=0; i<testCustomerArray.length; ++i) {
            testCustomerArray[i] = new Customer(testId + i, testName, testAddress);

            testRepo.saveCustomer(testCustomerArray[i]);
        }

        List<Customer> returnedCustomers = testRepo.getAllCustomers();
        LOG.info("Received [" + returnedCustomers.size() +
                 "] Customers from repository");

        for(Customer thisCustomer: testCustomerArray) {
            assertTrue(returnedCustomers.contains(thisCustomer));
        }
        LOG.info("Matched [" + testCustomerArray.length + "] Customers");
    }
    
    @Test (expected = CustomerNotFoundException.class)
    public void test_deleteCustomer() {
    	Customer[] testCustomerArray = new Customer[100];
        InMemCustomerDao testRepo = new InMemCustomerDao();

        for(int i=0; i<testCustomerArray.length; ++i) {
            testCustomerArray[i] = new Customer(testId + i, testName, testAddress);

            testRepo.saveCustomer(testCustomerArray[i]);
        }
        Customer removedCustomer = testCustomerArray[5];
    	testRepo.deleteCustomer(removedCustomer.getId());
    	LOG.info("Removed item from customer array");
    	testRepo.getCustomer(removedCustomer.getId());
    }
}

