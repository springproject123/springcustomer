package com.citi.training.customers.rest;

import javax.annotation.Priority;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.citi.training.customers.exceptions.CustomerNotFoundException;

@ControllerAdvice
@Priority(1)
public class DefaultExceptionHandler {
	
	private static Logger LOG = LoggerFactory.getLogger(DefaultExceptionHandler.class);

	@ExceptionHandler(value = {CustomerNotFoundException.class})
	public ResponseEntity<Object> customerNotFoundExceptionHandler(CustomerNotFoundException ex){
		LOG.warn(ex.toString());
		return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}", HttpStatus.NOT_FOUND);
	}
	
}
