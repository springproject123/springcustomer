package com.citi.training.customers.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.customers.dao.CustomerDao;
import com.citi.training.customers.model.Customer;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	private static Logger LOG = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	private CustomerDao customerRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Customer> getAll() {
		LOG.info("getAll was called");
		LOG.debug("This is  a debug");
		return customerRepository.getAllCustomers();
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Customer> save(@RequestBody Customer customer) {
		LOG.info("save was called: " + customer);
		customerRepository.saveCustomer(customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Customer get(@PathVariable int id) {
		LOG.info("get was called, id:" + id);
		return customerRepository.getCustomer(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)//used to give successful response code
	public void delete(@PathVariable int id) {
		LOG.info("delete was called: " + id);
		customerRepository.deleteCustomer(id);;		
	}
}
