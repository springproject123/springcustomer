package com.citi.training.customers.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.citi.training.customers.dao.CustomerDao;
import com.citi.training.customers.model.Customer;

@Component
public class Menu implements ApplicationRunner {
	
	@Autowired
    private CustomerDao customerDao;

    public void run(ApplicationArguments appArgs) {
        System.out.println("Creating some customers");
        String[] customerNames = {"Jim", "Jane", "Harry"};
        String[] customerAddresses = {"Belfast", "Armagh", "Portrush"};

        for(int i=0; i< customerNames.length; ++i) {
            Customer thisCustomer = new Customer(i,
                                              customerNames[i],
                                              customerAddresses[i]);

            customerDao.saveCustomer(thisCustomer);
        }
    }
}
