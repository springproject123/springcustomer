package com.citi.training.customers.exceptions;

@SuppressWarnings("serial")
public class CustomerNotFoundException extends RuntimeException {

	private String message;
	
	public CustomerNotFoundException(String message) {
		super();
		this.setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

