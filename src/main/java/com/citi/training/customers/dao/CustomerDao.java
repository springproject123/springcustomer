package com.citi.training.customers.dao;

import java.util.List;

import com.citi.training.customers.model.Customer;

public interface CustomerDao {
	
	void saveCustomer(Customer customer);

    Customer getCustomer(int id);

    List<Customer> getAllCustomers();
    
    void deleteCustomer(int id);
}
