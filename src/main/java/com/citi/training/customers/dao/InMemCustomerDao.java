package com.citi.training.customers.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.citi.training.customers.model.Customer;
import com.citi.training.customers.exceptions.CustomerNotFoundException;

@Component
public class InMemCustomerDao implements CustomerDao {

	private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();
	
	@Override
	public void saveCustomer(Customer customer) {
		allCustomers.put(customer.getId(), customer);
	}

	@Override
	public Customer getCustomer(int id) {
		Customer customer = new Customer();
		customer = allCustomers.get(id);
		if (customer == null) {
			throw new CustomerNotFoundException("Customer has not been found");
		}
		return customer;
	}

	@Override
	public List<Customer> getAllCustomers() {
		return new ArrayList<Customer>(allCustomers.values());
	}

	@Override
	public void deleteCustomer(int id) {
		Customer customer = allCustomers.remove(id);
    	if (customer == null) {
    		throw new CustomerNotFoundException("Customer could not be deleted");
    	}
	}

}
